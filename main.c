#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MEASURE_TIME
//#define GENERATE_FILES
#define SEARCH_QUERY_NUMBER 10000000
#define INPUT_NUMBER 10000000

#include "avl_tree.h"
#include "avl_helpers.h"

int main()
{
#ifndef GENERATE_FILES
	avl_node* root = NULL;
	FILE* inputfile = fopen("numbers.txt", "r");
	FILE* tosearchfile = fopen("tosearch.txt", "r");
	FILE* outfile = fopen("out.txt", "w");
	int number = 0;
	int* search = (int*) malloc(SEARCH_QUERY_NUMBER * sizeof(int));
	avl_node* foundnode = NULL;
	int i = 0;
	printf("Allocated memory\n");
#ifdef MEASURE_TIME
	clock_t t1 = 0;
	clock_t t2;
	int diff = 0;
#endif //MEASURE_TIME
	while (!feof(inputfile))
	{
		fscanf(inputfile, "%d", &number);
		root = avl_add_node(root, number);
	}
	while (!feof(tosearchfile))
	{
		fscanf(tosearchfile, "%d", &number);
		search[i++] = number;
	}
#ifdef MEASURE_TIME
	printf("Searching\n");
	t1 = clock();
#endif //MEASURE_TIME
	for (i = 0; i < SEARCH_QUERY_NUMBER; ++i)
	{
		search[i] = avl_search_node(root, search[i]) ? 1 : 0;
	}
#ifdef MEASURE_TIME
	diff = clock() - t1;
	printf("Passed time = %f s\n", diff * 1.0f / CLOCKS_PER_SEC);
#endif //MEASURE_TIME
	// output
	for (i = 0; i < SEARCH_QUERY_NUMBER; ++i)
	{
		fprintf(outfile, "%d\n", search[i]);
	}
	free(search);
	fclose(inputfile);
	fclose(tosearchfile);
	fclose(outfile);
	avl_destroy_tree(root);
#else
	FILE* inputfile = fopen("numbers.txt", "w");
	FILE* tosearchfile = fopen("tosearch.txt", "w");
	int i = 0;
	int inputcount = INPUT_NUMBER; // 10 mln
	int tosearchcount = SEARCH_QUERY_NUMBER; // 1 mln
	srand(time(NULL));
	for (; i < inputcount; ++i)
	{
		fprintf(inputfile, "%d\n", rand() % inputcount);
	}
	srand(time(NULL));
	for (i = 0; i < tosearchcount; ++i)
	{
		fprintf(tosearchfile, "%d\n", (rand() % inputcount) << 1);
	}
	fclose(inputfile);
	fclose(tosearchfile);
#endif //GENERATE_FILES
	return 0;
}