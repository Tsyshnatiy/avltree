#include <stdio.h>
#include "avl_helpers.h"

#ifdef PRINT_BFS
#include <math.h>	//for log2
#include <queue>	// for printing
using namespace std; // for printing

static unsigned char height(avl_node* n)
{
	return n ? n->height : 0;
}

static int bfactor(avl_node* n)
{
	return height(n->right) - height(n->left);
}
int avl_bfs(avl_node* root)
{
	queue<avl_node*> trip;
	long nodecounter = 1;
	int lasttreelevel = 0;
	int newtreelevel = 1;
	trip.push(root);
	while(!(trip.empty()))
	{
		avl_node* nodeptr = trip.front();
		if (nodeptr)
		{
			printf("%5d(%d)", nodeptr->key, bfactor(nodeptr));
			trip.push(nodeptr->left);
			trip.push(nodeptr->right);
		}
		else
		{
			printf("%5s", "N");
		}
		trip.pop();
		nodecounter++;
		newtreelevel = (int)log2(nodecounter);
		if (newtreelevel > lasttreelevel)
		{
			printf("\n");
			lasttreelevel = newtreelevel;
		}
	}
	printf("\n");
	return 0;
}
#endif

int avl_inorder_traverse(avl_node* root)
{
	if (!root)
	{
		return 0;
	}

	if (root->left)
	{
		avl_inorder_traverse(root->left);
	}
	printf("%3d", root->key);
	if (root->right)
	{
		avl_inorder_traverse(root->right);
	}
	return 0;
}