#include <stdio.h>
#include <stdlib.h>
#include "avl_tree.h"

static unsigned char height(avl_node* n)
{
	return n ? n->height : 0;
}

static int bfactor(avl_node* n)
{
	return height(n->right) - height(n->left);
}

static void fix_height(avl_node* node)
{
	unsigned char hl = height(node->left);
	unsigned char hr = height(node->right);
	node->height = hl > hr ? hl : hr + 1;
}

static avl_node* avl_rotate_right(avl_node* p)
{
	avl_node* q = p->left;
	p->left = q->right;
	q->right = p;
	fix_height(p);
	fix_height(q);
	return q; // return new subtree root
}

static avl_node* avl_rotate_left(avl_node* q)
{
	avl_node* p = q->right;
	q->right = p->left;
	p->left = q;
	fix_height(q);
	fix_height(p);
	return p; // return new subtree root
}

static avl_node* avl_balance(avl_node* node)
{
	fix_height(node);
	if (bfactor(node) == 2) //right subtree is bigger than left
	{
		if (bfactor(node->right) < 0)
		{
			node->right = avl_rotate_right(node->right);
		}
		return avl_rotate_left(node);
	}
	if (bfactor(node) == -2) //left subtree is bigger than left
	{
		if (bfactor(node->left) > 0)
		{
			node->left = avl_rotate_left(node->left);
		}
		return avl_rotate_right(node);
	}
	
	return node; // no need in balancing
}

static avl_node* avl_removemin(avl_node* p)
{
    if(!(p->left))
        return p->right;
    p->left = avl_removemin(p->left);
    return avl_balance(p);
}

avl_node* avl_add_node(avl_node* p, int key)
{
	avl_node* result;
	if (!p)
	{
		result = (avl_node*) malloc(sizeof(avl_node));
		result->height = 1;
		result->left = NULL;
		result->right = NULL;
		result->key = key;
		return result;
	}
	if (key < p->key)
	{
		p->left = avl_add_node(p->left, key);
	}
	else
	{
		p->right = avl_add_node(p->right, key);
	}
	return avl_balance(p);
}

avl_node* avl_search_node(avl_node* root, int key)
{
	if (root)
	{
		if (key < root->key)
		{
			return avl_search_node(root->left, key);
		}
		else if (key > root->key)
		{
			return avl_search_node(root->right, key);
		}
		else
		{
			return root;
		}
	}
	return NULL;
}

avl_node* avl_findmin(avl_node* p)
{
    return p->left ? avl_findmin(p->left) : p;
}

avl_node* avl_remove_node(avl_node* p, int key)
{
    if(!p) 
	{
		return 0;
	}
    if(key < p->key)
	{
        p->left = avl_remove_node(p->left, key);
	}
    else if(key > p->key)
	{
        p->right = avl_remove_node(p->right, key);	
	}
    else //  k == p->key 
    {
        avl_node* q = p->left;
        avl_node* r = p->right;
        free(p);
        if(!r)
		{
			return q;
		}
        avl_node* min = avl_findmin(r);
        min->right = avl_removemin(r);
        min->left = q;
        return avl_balance(min);
    }
    return avl_balance(p);
}

int avl_destroy_tree(avl_node* root)
{
	if (!root)
	{
		return 0;
	}
	if (root->left)
	{
		avl_destroy_tree(root->left);
	}
	if (root->right)
	{
		avl_destroy_tree(root->right);
	}
	free(root);
	return 0;
}