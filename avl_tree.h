#ifndef AVL_TREE_H
#define AVL_TREE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct avl_node
{
	struct avl_node* left;
	struct avl_node* right;
	unsigned char height;
	int key;
}avl_node;

avl_node* avl_add_node(avl_node* p, int key);

avl_node* avl_search_node(avl_node* root, int key);

avl_node* avl_findmin(avl_node* p);

avl_node* avl_remove_node(avl_node* p, int key);

int avl_destroy_tree(avl_node* root);

#ifdef __cplusplus
}
#endif
#endif //AVL_TREE_H