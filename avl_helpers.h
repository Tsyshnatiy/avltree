#ifndef AVL_HELPERS_H
#define AVL_HELPERS_H

#include "avl_tree.h"
//#define PRINT_BFS
#ifdef PRINT_BFS
//C++ only! 
// avl_bfs uses queue
int avl_bfs(avl_node* root);
int avl_inorder_traverse(avl_node* root);
#endif //PRINT_BFS
#endif //AVL_HELPERS_H