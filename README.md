# README #
This repo implements avl tree basics.
You can add, remove, search, find min node and etc.
Merge is not implemented.
All the code except of printing is plain C.
Also repo contains pdf with avl theory.

### How do I get set up? ###
gcc avl_tree.c avl_helpers.c main.c -o avl
If you want printing functions, go to avl_helpers.h and define PRINT_BFS.
Then build via 
g++ avl_tree.c avl_helpers.c main.c -o avl
g++ is because bfs uses queue class. Rest of the code is plain C.